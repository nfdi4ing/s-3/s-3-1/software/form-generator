<template>
  <div
    v-if="componentDefinition !== null && values"
    :class="componentDefinition + ' mb-3'"
    :style="cssProps"
  >
    <b-form-group
      :class="{
        invisible: !fixedValueMode && invisible,
      }"
      label-cols-sm="3"
      label-align-sm="right"
      :label-class="
        'application-profile-label' +
        (required ? ' mandatory' : '') +
        ' d-inline-block text-end px-1'
      "
      class
    >
      <!-- Label Template -->
      <template #label>
        <!-- Label -->
        <span>{{ label }}</span>

        <div v-if="description" class="d-inline-flex ms-1">
          <!-- Information Circle Icon -->
          <i-bi-info-circle v-b-popover.hover.bottom="description" />
        </div>
      </template>

      <b-button-toolbar v-for="(entry, entryIndex) in values" :key="entryIndex">
        <component
          :is="componentDefinition"
          class="wrapper-input"
          :wrapper-dataset="wrapperDataset"
          :locked="locked"
          :required="required"
          :fixed-value-mode="fixedValueMode"
          :fixed-values="fixedValues"
          :disabled-mode="disabledMode"
          :language-code="languageLocale"
          :entry="entry"
          :data-type="dataType"
          :class-object="classObject"
          :list="list"
          :list-labels="listLabels"
          :single-line="singleLine"
          :state="state"
          :attached-metadata="attachedMetadata[entryIndex]"
          :node-import="nodeImport"
          :node-name="nodeName"
          :target-class="targetClass"
          :class-receiver="classReceiver"
          :instance-receiver="instanceReceiver"
          :user-receiver="userReceiver"
          @update:metadata-value="inputValue(entryIndex, $event)"
          @update:fixed-values="inputFullFixedValues"
          @triggerUpdateDataset="triggerUpdateDataset"
          @triggerValidation="triggerValidation"
        />
        <WrapperButtonGroup
          :disabled-mode="disabledMode"
          :entry-index="entryIndex"
          :field-can-be-invisible="fieldCanBeInvisible"
          :fixed-value-disabled="fixedValueDisabled"
          :fixed-value-mode="fixedValueMode"
          :insert-new-field-button-disabled="insertNewFieldButtonDisabled"
          :invisible="invisible"
          :locked="locked"
          :remove-field-button-disabled="removeFieldButtonDisabled"
          @changeLockable="changeLockable"
          @changeVisibility="changeVisibility"
          @insertNewFields="insertNewFields"
          @removeField="removeField"
        />
      </b-button-toolbar>
      <b-button
        v-if="values.length === 0"
        :disabled="disabledMode || insertNewFieldButtonDisabled"
        class="innerButton float-end"
        variant="outline-secondary"
        @click.prevent="insertNewFields(1)"
      >
        <i-mdi-plus />
      </b-button>
      <b-form-invalid-feedback :state="state">
        <div v-for="(message, index) in errorMessages[nodeName]" :key="index">
          {{ message.value }}
        </div>
      </b-form-invalid-feedback>
    </b-form-group>
  </div>
</template>

<script lang="ts">
import InputCombobox from './InputCombobox.vue';
import InputTextField from './InputTextField.vue';
import InputDatePicker from './InputDatePicker.vue';
import InputBooleanCombobox from './InputBooleanCombobox.vue';
import InputList from './InputList.vue';
import InputShape from './InputShape.vue';

import type {
  Dataset,
  NamedNode,
  Quad_Object,
  Quad_Subject,
} from '@rdfjs/types';
import prefixes from '@zazuko/prefixes/prefixes';
import factory from 'rdf-ext';
import {
  getObject,
  getObjectList,
  retrieveChildren,
  retrieveInstance,
} from '@/util/linkedData';
import type { FixedValues } from '@/types/fixedValues';
import type { User } from '@/types/user';
import { useFixedValues } from '@/composables/fixedValueWrapper';

export default defineComponent({
  name: 'WrapperInput',
  components: {
    InputTextField,
    InputCombobox,
    InputDatePicker,
    InputBooleanCombobox,
    InputList,
    InputShape,
  },
  props: {
    wrapperDataset: {
      required: true,
      type: Object as PropType<Dataset>,
    },
    metadata: {
      required: true,
      type: Object as PropType<Dataset>,
    },
    metadataSubject: {
      default: () => factory.blankNode(),
      type: Object as PropType<Quad_Subject>,
    },
    fixedValues: {
      default: () => ({}),
      type: Object as PropType<FixedValues>,
    },
    fixedValueMode: {
      default: false,
      type: Boolean,
    },
    disabledMode: {
      default: false,
      type: Boolean,
    },
    languageLocale: {
      default: 'en',
      type: String,
    },
    targetClass: {
      required: true,
      type: Object as PropType<Quad_Object>,
    },
    templateMode: {
      default: false,
      type: Boolean,
    },
    errorMessages: {
      default: () => ({}),
      type: Object as PropType<{ [nodeName: string]: Quad_Object[] }>,
    },
    classReceiver: {
      default: () => retrieveChildren,
      type: Function as PropType<typeof retrieveChildren>,
    },
    instanceReceiver: {
      default: () => retrieveInstance,
      type: Function as PropType<typeof retrieveInstance>,
    },
    userReceiver: {
      default: () => {
        return async () => ({});
      },
      type: Function as PropType<() => Promise<User>>,
    },
    property: {
      required: true,
      type: Object as PropType<Quad_Subject>,
    },
  },
  setup(props, { emit }) {
    const { wrapperDataset, property } = toRefs(props);

    const nodeName = computed(() => {
      const names = getObject(
        property.value,
        factory.namedNode(prefixes.sh + 'path'),
        wrapperDataset.value,
      );
      if (!names.length) {
        return '';
      }
      return names[0].object.value;
    });

    const minCount = computed(() => {
      const minCounts = getObject(
        property.value,
        factory.namedNode(prefixes.sh + 'minCount'),
        wrapperDataset.value,
      );
      if (!minCounts.length) {
        return 0;
      }
      return Number(minCounts[0].object.value);
    });

    const required = computed(() => {
      return minCount.value >= 1;
    });

    const values = ref<Quad_Object[]>([]);
    const fixedValueUse = useFixedValues(
      props,
      wrapperDataset,
      emit,
      nodeName,
      required,
      values,
    );

    return { minCount, nodeName, required, values, ...fixedValueUse };
  },
  computed: {
    attachedMetadata(): Dataset[] {
      return this.values.map((entry) => {
        const collectedNodes = [entry];
        const toLookAtNodes = [entry];
        const attachedMetadata = factory.dataset() as unknown as Dataset;
        while (toLookAtNodes.length > 0) {
          const currentNode = toLookAtNodes.pop();
          const currentAttachedMetadata = Array.from(
            this.metadata.match(currentNode),
          );
          attachedMetadata.addAll(currentAttachedMetadata);
          toLookAtNodes.push(
            ...currentAttachedMetadata
              .filter(
                (currentOldValue) =>
                  (currentOldValue.object.termType === 'NamedNode' ||
                    currentOldValue.object.termType === 'BlankNode') &&
                  !collectedNodes.some(
                    (collectedNode) =>
                      collectedNode.value === currentOldValue.object.value,
                  ),
              )
              .map((quad) => quad.object),
          );
          collectedNodes.push(
            ...currentAttachedMetadata.map((quad) => quad.object),
          );
        }
        return attachedMetadata;
      });
    },
    classObject(): NamedNode<string> | null {
      const classes = this.wrapperDataset.match(
        this.property,
        factory.namedNode(prefixes.sh + 'class'),
      );
      if (classes.size) {
        for (const classEntry of classes) {
          return classEntry.object as NamedNode<string>;
        }
      }
      return null;
    },
    componentDefinition(): string | null {
      // Template mode only shows text fields
      if (this.templateMode && !this.isNode) {
        return 'InputTextField';
      }
      if (this.dataType) {
        if (this.dataType.value === prefixes.xsd + 'date') {
          return 'InputDatePicker';
        } else if (this.dataType.value === prefixes.xsd + 'boolean') {
          return 'InputBooleanCombobox';
        }
        return 'InputTextField';
      } else {
        if (this.isNode) {
          return 'InputShape';
        }
        if (this.isList) {
          return 'InputList';
        }
        if (this.classObject) {
          return 'InputCombobox';
        }
        if (this.hasValues.length > 0) {
          return 'InputTextField';
        }
      }
      return null;
    },
    cssProps(): {
      '--wrapperInputWidth': string;
      '--wrapperInputRightMargin': string;
    } {
      return {
        // Formula: 100% - (n.Buttons)*(Button Width) - (--wrapperInputRightMargin)
        '--wrapperInputWidth': this.fixedValueMode
          ? 'calc(100% - 181px - 1rem)'
          : 'calc(100% - 61px - 5px)',
        '--wrapperInputRightMargin': this.fixedValueMode ? '1rem' : '5px',
      };
    },
    dataType(): NamedNode | undefined {
      const dataTypes = this.wrapperDataset.match(
        this.property,
        factory.namedNode(prefixes.sh + 'datatype'),
      );
      for (const dataType of dataTypes) {
        return dataType.object as NamedNode;
      }
      return undefined;
    },
    description(): string | undefined {
      const descriptions = getObject(
        this.property,
        factory.namedNode(prefixes.sh + 'description'),
        this.wrapperDataset,
        this.languageLocale,
      );
      for (const description of descriptions) {
        return description.object.value;
      }
      return undefined;
    },
    hasValues(): Quad_Object[] {
      const hasValues = this.wrapperDataset.match(
        this.property,
        factory.namedNode(prefixes.sh + 'hasValue'),
      );
      const valueObjects: Quad_Object[] = [];
      for (const hasValue of hasValues) {
        valueObjects.push(hasValue.object);
      }
      return valueObjects;
    },
    insertNewFieldButtonDisabled(): boolean {
      if (
        this.values.length === 0 ||
        ((!this.maxCount || this.values.length < this.maxCount) &&
          this.values.every((entry) => entry.value !== ''))
      ) {
        return false;
      } else {
        return true;
      }
    },
    isList(): boolean {
      return (
        this.wrapperDataset.match(
          this.property,
          factory.namedNode(prefixes.sh + 'in'),
        ).size > 0
      );
    },
    isNode(): boolean {
      return (
        this.wrapperDataset.match(
          this.property,
          factory.namedNode(prefixes.sh + 'node'),
        ).size > 0 ||
        this.wrapperDataset.match(
          this.property,
          factory.namedNode(prefixes.sh + 'qualifiedValueShape'),
        ).size > 0
      );
    },
    label(): string {
      const labels = getObject(
        this.property,
        factory.namedNode(prefixes.sh + 'name'),
        this.wrapperDataset,
        this.languageLocale,
      );
      if (!labels.length) {
        return this.nodeName;
      }
      return labels[0].object.value;
    },
    list(): Quad_Object[] {
      if (this.isList) {
        return getObjectList(
          this.property,
          factory.namedNode(prefixes.sh + 'in'),
          this.wrapperDataset,
        );
      }
      return [];
    },
    listLabels(): Quad_Object[][] {
      const labels: Quad_Object[][] = [];
      for (const entry of this.list) {
        if (entry.termType !== 'Literal') {
          const labelQuads = getObject(
            entry,
            factory.namedNode(prefixes.rdfs + 'label'),
            this.wrapperDataset,
          );
          labels.push(labelQuads.map((labelQuad) => labelQuad.object));
        }
      }
      return labels;
    },
    maxCount(): number | undefined {
      const maxCounts = getObject(
        this.property,
        factory.namedNode(prefixes.sh + 'maxCount'),
        this.wrapperDataset,
      );
      if (!maxCounts.length) {
        return undefined;
      }
      return Number(maxCounts[0].object.value);
    },
    minLength(): number {
      const minLengths = getObject(
        this.property,
        factory.namedNode(prefixes.sh + 'minLength'),
        this.wrapperDataset,
      );
      if (!minLengths.length) {
        return 0;
      }
      return Number(minLengths[0].object.value);
    },
    nodeImport(): string {
      const names = getObject(
        this.property,
        factory.namedNode(prefixes.sh + 'node'),
        this.wrapperDataset,
      );
      names.push(
        ...getObject(
          this.property,
          factory.namedNode(prefixes.sh + 'qualifiedValueShape'),
          this.wrapperDataset,
        ),
      );
      if (!names.length) {
        return '';
      }
      return names[0].object.value;
    },
    removeFieldButtonDisabled(): boolean {
      if (this.values.length > this.minCount) {
        return false;
      } else {
        return true;
      }
    },
    singleLine(): boolean {
      return !this.wrapperDataset.some(
        (quad) =>
          quad.subject.value === this.property.value &&
          quad.predicate.value === 'http://datashapes.org/dash#singleLine' &&
          (quad.object.value === 'false' || quad.object.value === '0'),
      );
    },
    state(): boolean | null {
      if (this.errorMessages[this.nodeName]?.length) {
        return false;
      } else if (this.values.some((entry) => entry.value !== '')) {
        return true;
      }
      return null;
    },
    // TODO: Currently, then handling of multiple values is a bit weird
    // e.g. if you enter the same value, one disappears
    // This is due to a limitation that a wrapperDataset can only contain one ?s ?p ?o triple
    // Try to think how to deal with this limitation and resolve the weirdness
    metadataValues(): Quad_Object[] {
      return Array.from(
        this.metadata.match(
          this.metadataSubject,
          factory.namedNode(this.nodeName),
        ),
      ).map((quad) => this.cloneValue(quad.object));
    },
  },
  watch: {
    metadata() {
      this.setValuesFromMetadataValues();
      this.fillMetadata();
    },
  },
  beforeMount() {
    this.initMetadata();
    this.initFixedValues();
    this.initDefaultValues();
    this.initHasValues();
  },
  methods: {
    fillMetadata() {
      // Set values if they don't exist
      if (!this.values.length) {
        const numberOfNewFields = this.isNode ? 0 : 1;
        // Setting more than one will not work since a wrapperDataset does not support multiple triples
        // which are the same ?s ?p ?o
        this.insertNewFields(numberOfNewFields);
      }
    },
    initHasValues() {
      if (this.hasValues && this.hasValues.length > 0) {
        this.updateMetadataValue(
          this.hasValues.map((object) => this.createRelevantEntry(object)),
          false,
        );
      }
    },
    initMetadata() {
      this.values = [...this.metadataValues];
      this.fillMetadata();
    },
    triggerUpdateDataset(value: Quad_Object, wrapperDataset: Dataset) {
      this.$emit('triggerUpdateDataset', value, wrapperDataset);
    },
    inputValue(entryKey: number, value: Quad_Object) {
      const newValues = [...this.values];
      // Clone for breaking reactivity
      newValues[entryKey] = this.cloneValue(value);
      this.updateMetadataValue(newValues);
    },
    insertNewFields(numberOfNewFields = 1) {
      const newValues = [...this.values];
      for (let i = 0; i < numberOfNewFields; i++) {
        newValues.push(this.createRelevantEntry());
      }
      this.updateMetadataValue(newValues, false);
    },
    cloneValue(value: Quad_Object) {
      if (value.termType === 'NamedNode') {
        return factory.namedNode(value.value);
      } else if (value.termType === 'Literal') {
        return factory.literal(
          value.value,
          value.language ? value.language : value.datatype,
        );
      }
      return value;
    },
    createRelevantEntry(value: Quad_Object | undefined = undefined) {
      if (value) {
        return this.cloneValue(value);
      }
      let entry: Quad_Object = factory.literal('', this.dataType);
      if (this.isNode) {
        entry = factory.blankNode();
      } else if (!this.dataType) {
        entry = factory.namedNode('');
      }
      return entry;
    },
    removeField(entryKey: number) {
      const newValues = [...this.values];
      const removedEntries = newValues.splice(entryKey, 1);
      if (this.isNode) {
        for (const removedEntry of removedEntries) {
          this.triggerUpdateDataset(
            removedEntry,
            factory.dataset() as unknown as Dataset,
          );
        }
      }
      this.updateMetadataValue(newValues);
    },
    setValuesFromMetadataValues() {
      // What values will be kept from this.values
      const keptValues = this.values.map((entry) =>
        this.metadataValues.some(
          (metadataEntry) => entry.value === metadataEntry.value,
        )
          ? entry
          : false,
      );
      // What values are new in this.metadataValues
      const newValues = this.metadataValues.filter(
        (metadataEntry) =>
          !keptValues.some(
            (entry) => entry !== false && entry.value === metadataEntry.value,
          ),
      );

      let currentAddKey = 0;
      const removeKeys: number[] = [];
      // Update every value which is not kept with a new value
      for (let i = 0; i < this.values.length; i++) {
        if (keptValues[i] === false) {
          if (currentAddKey < newValues.length) {
            this.values[i] = newValues[currentAddKey];
            currentAddKey++;
          } else {
            removeKeys.push(i);
          }
        }
      }
      // Add values which have not been added
      for (let i = currentAddKey; i < newValues.length; i++) {
        this.values.push(newValues[i]);
      }

      // Create a list with every to keep and new value
      const setNewValues: Quad_Object[] = [];
      for (let i = 0; i < this.values.length; i++) {
        if (!removeKeys.includes(i)) {
          setNewValues.push(this.values[i]);
        }
      }

      this.values = setNewValues;
    },
    triggerValidation() {
      this.$emit('triggerValidation');
    },
  },
});
</script>

<style scoped>
.wrapper-input {
  width: var(--wrapperInputWidth);
  margin-right: var(--wrapperInputRightMargin);
}
.wrapper-input-button {
  width: 21px;
}
</style>
