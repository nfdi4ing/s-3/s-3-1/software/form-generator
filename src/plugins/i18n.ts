import { createI18n } from 'vue-i18n';

import locale from '@/locale';

const i18n = createI18n({
  legacy: false,
  locale: 'en',
  fallbackLocale: 'en',
  messages: locale,
  silentFallbackWarn: true,
  globalInjection: true,
});

export default i18n;
