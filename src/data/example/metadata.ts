export const listMetadata = `@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .

_:b3 a foaf:Person ;
	foaf:mbox "example@example.com" ;
	foaf:firstName "Max" ;
	foaf:lastName "Mustermann" ;
	foaf:title "Dr." ;
	<http://w3id.org/nfdi4ing/metadata4ing#orcidId> "1234" ;
	<http://example.org/fruits> "Banana" .`;

export const dropShapeAnalysisMetadata = `@prefix dc: <http://purl.org/dc/terms/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix ns0: <http://purl.obolibrary.org/obo/> .
@prefix ns1: <http://purl.allotrope.org/ontologies/result#> .

<https://hdl.handle.net/21.11102/0526503b-68c2-4793-8cf4-50e8ecadb15b@path=%2FImage20220912151316.png>
  a <https://purl.org/coscine/ap/sfb985/dropShapeAnalysis/> ;
  dc:created "2022-12-22"^^xsd:date ;
  dc:contributor "Tester"^^xsd:string ;
  dc:creator "Tester"^^xsd:string ;
  ns0:CHMO_0001163 <http://purl.org/coscine/vocabularies/sfb985/measurementType#1> ;
  ns0:CHMO_0002820 2.0 ;
  ns1:AFR_0001118 "23"^^xsd:string ;
  ns0:BFO_0000051 <http://purl.org/coscine/vocabularies/sfb985/capillaryTipType#1> ;
  ns0:IAO_0000129 "ert"^^xsd:string ;
  ns0:OBI_0000968 <http://purl.org/coscine/vocabularies/sfb985/instrument#0> .`;

export const dataCiteMetadata = `@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix schema: <http://schema.org/> .
@prefix aps: <https://purl.org/coscine/ap/> .

_:b1 a <https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/> ;
	dcterms:title "Test" ;
	schema:description "test" ;
	dcterms:subject "Earth and Environmental Sciences" ;
	dcterms:creator [
		rdf:type <https://purl.org/coscine/ap/Dataverse_Citation_Metadata/author/> ;
		dcterms:creator "Cool name" ;
		schema:affiliation "Somewhere" ;
		<http://purl.org/spar/datacite/AgentIdentifierScheme> "{{http://purl.org/spar/datacite/AgentIdentifierScheme}}" ;
		<http://purl.org/spar/datacite/AgentIdentifier> "1234" ;
	] ;
	schema:contactPoint [
		rdf:type <https://purl.org/coscine/ap/Dataverse_Citation_Metadata/pointOfContact/> ;
		schema:affiliation "Otherwhere" ;
		foaf:name "Another Cool Name" ;
		schema:email "example@example.com" ;
	] .`;
