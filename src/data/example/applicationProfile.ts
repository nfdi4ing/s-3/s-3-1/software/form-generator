export const listApplicationProfile = `@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix aps: <https://aims-projekt.de/ap/> .

<https://aims-projekt.de/ap/person/> dcterms:created "2022-10-10"^^xsd:date ;
	dcterms:creator "Benedikt Heinrichs" ;
	dcterms:description "Eine Person."@de ;
	dcterms:rights "test" ;
	dcterms:title "Person"@de, "Person"@en ;
	a sh:NodeShape ;
	owl:imports <https://aims-projekt.de/ap/agent/> ;
	sh:closed false ;
	sh:node <https://aims-projekt.de/ap/agent/> ;
	sh:property [
		sh:datatype xsd:string ;
		sh:minCount 1 ;
		sh:name "First Name"@en, "Vorname"@de ;
		sh:defaultValue "{ME}" ;
		sh:order 1 ;
		sh:path foaf:firstName ;
	], [
		sh:datatype xsd:string ;
		sh:minCount 1 ;
		sh:name "Last Name"@en, "Nachname"@de ;
		sh:defaultValue "{ME}" ;
		sh:order 2 ;
		sh:path foaf:lastName ;
	], [
		sh:datatype xsd:string ;
		sh:name "Title"@en, "Titel"@de ;
		sh:order 3 ;
		sh:path foaf:title ;
	], [
		sh:datatype xsd:string ;
		sh:name "ORCID Id"@de, "ORCID Id"@en ;
		sh:order 4 ;
		sh:path <http://w3id.org/nfdi4ing/metadata4ing#orcidId> ;
	], [
		sh:minCount 1 ;
		sh:name "Früchte"@de, "Fruits"@en ;
		sh:order 5 ;
		sh:path <http://example.org/fruits> ;
		sh:in (
			"Banana"
			"Apple"
			"Orange"
		) ;
	] ;
	sh:targetClass foaf:Person .

<https://aims-projekt.de/ap/agent/> dcterms:created "2022-10-10"^^xsd:date ;
	dcterms:creator "Benedikt Heinrichs" ;
	dcterms:description "Ein Agent."@de ;
	dcterms:rights "test" ;
	dcterms:title "Agent"@de, "Agent"@en ;
	a sh:NodeShape ;
	sh:closed false ;
	sh:property [
		sh:datatype xsd:string ;
		sh:minCount 1 ;
		sh:minLength 3 ;
		sh:name "E-Mail"@de, "Email"@en ;
		sh:order 1 ;
		sh:path foaf:mbox ;
		sh:message "This column needs a valid email address."@en ;
	] ;
	sh:targetClass foaf:Agent .`;

export const dropShapeAnalysis = `@base <https://purl.org/coscine/ap/sfb985/dropShapeAnalysis/> .

@prefix dash: <http://datashapes.org/dash#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix qudt: <http://qudt.org/schema/qudt/> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix ipc: <http://purl.org/coscine/terms/IPC#> .
@prefix afr: <http://purl.allotrope.org/ontologies/result#> .
@prefix afm: <http://purl.allotrope.org/ontologies/material#> .
@prefix obo: <http://purl.obolibrary.org/obo/> .
@prefix repr: <https://w3id.org/reproduceme#> .
@prefix om2: <http://www.ontology-of-units-of-measure.org/resource/om-2/> .
@prefix ebucore: <http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#> .
@prefix coscinedropShapeAnalysis: <https://purl.org/coscine/ap/sfb985/dropShapeAnalysis#> .


<https://purl.org/coscine/ap/sfb985/dropShapeAnalysis/>
  dcterms:license <http://spdx.org/licenses/MIT> ;
  dcterms:publisher <https://itc.rwth-aachen.de/> ;
  dcterms:rights "Copyright © 2022 IT Center, RWTH Aachen University" ;
  dcterms:title "Drop Shape Analysis"@en, "Drop Shape Analysis"@de ;

  a sh:NodeShape ;
  sh:targetClass <https://purl.org/coscine/ap/sfb985/dropShapeAnalysis/> ;
  sh:closed true ; 

  sh:property [
    sh:path rdf:type ;
  ] ;

  sh:property coscinedropShapeAnalysis:personResponsible ;
  sh:property coscinedropShapeAnalysis:operator ;
  sh:property coscinedropShapeAnalysis:sampleID ;
  sh:property coscinedropShapeAnalysis:samplePID; 
  sh:property coscinedropShapeAnalysis:measurementDate ; 
  sh:property coscinedropShapeAnalysis:measurementType ;
  sh:property coscinedropShapeAnalysis:oscillatingDropMeasurementType ;
  sh:property coscinedropShapeAnalysis:interface ;
  sh:property coscinedropShapeAnalysis:oilType ;
  sh:property coscinedropShapeAnalysis:instrument ;
  sh:property coscinedropShapeAnalysis:accessoriesUsed ; 
  sh:property coscinedropShapeAnalysis:capillaryTipType ;
  sh:property coscinedropShapeAnalysis:softwareVersion ;
  sh:property coscinedropShapeAnalysis:comment ;
  sh:property coscinedropShapeAnalysis:sampleConcentration;
  sh:property coscinedropShapeAnalysis:elabID;

  .

coscinedropShapeAnalysis:personResponsible 
  sh:path dcterms:creator ;
  sh:order 0 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:minLength 1 ;
  sh:datatype xsd:string ;
  sh:name "Person Responsible"@en, "Ersteller"@de ;
.

coscinedropShapeAnalysis:operator
  sh:path dcterms:contributor ;
  sh:order 1 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Operator"@en, "Operator"@de;
. 
coscinedropShapeAnalysis:sampleID
  sh:path afr:AFR_0001118 ;
  sh:order 2 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ; 
  sh:name "Sample ID"@en, "Proben ID"@de ;
.

coscinedropShapeAnalysis:samplePID
  sh:path dcterms:identifier ;
  sh:order 3 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ; 
  sh:name "SFB 985 Sample Management PID"@en, "SFB 985 Probenmanagement PID"@de ;
.

coscinedropShapeAnalysis:measurementDate
  sh:path dcterms:created ; 
  sh:order 4 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:date ;
  sh:name "Date of Measurement"@en, "Messdatum"@de ;
.

coscinedropShapeAnalysis:instrument
  sh:path obo:OBI_0000968 ; 
  sh:order 5 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:name "Instrument"@en, "Instrument"@de ;
  sh:class <http://purl.org/coscine/vocabularies/sfb985/instrument> ;
.

coscinedropShapeAnalysis:accessoriesUsed
  sh:path obo:RO_0002180 ; 
  sh:order 6 ;
  sh:name "Accessories"@en, "Accesoires"@de ;
  sh:class <http://purl.org/coscine/vocabularies/sfb985/accessories>  ;
.


coscinedropShapeAnalysis:capillaryTipType
  sh:path obo:BFO_0000051 ; 
  sh:order 7 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:name "Type of capillary tip"@en, "Typ Kapillarspitze"@de ;
  sh:class <http://purl.org/coscine/vocabularies/sfb985/capillaryTipType> ;
.

coscinedropShapeAnalysis:measurementType
  sh:path obo:CHMO_0001163 ;
  sh:order 8 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:name "Measurement Type"@en, "Art der Messung"@de ;
  sh:class <http://purl.org/coscine/vocabularies/sfb985/measurementType> ;
.

coscinedropShapeAnalysis:oscillatingDropMeasurementType
  sh:path obo:OBI:0000070 ;
  sh:order 9 ;
  sh:maxCount 1 ;
  sh:name "Oscillating drop measurement type"@en, "Oscillating Drop Messart"@de ;
  sh:class <http://purl.org/coscine/vocabularies/sfb985/oscillatingDropMeasurementType> ;
.

coscinedropShapeAnalysis:sampleConcentration 
  sh:path obo:CHMO_0002820 ;
  sh:order 10 ;
  sh:minCount 1 ;
  sh:maxCount 1 ; 
  sh:datatype xsd:decimal ;
  sh:name "Sample Concentration [wt%]"@en, "Probenkonzentration [Massen-%]"@de ;
  qudt:Unit unit:PERCENT;
.

coscinedropShapeAnalysis:interface
  sh:path obo:ENVO_01001684 ; 
  sh:order 11 ;
  sh:maxCount 1 ;
  sh:name "Interface type"@en, "Grenzfläche"@de ;
  sh:class <http://purl.org/coscine/vocabularies/sfb985/interface> ;
.

coscinedropShapeAnalysis:oilType
  sh:path afm:AFM_0000403 ; 
  sh:order 12 ;
  sh:maxCount 1 ;
  sh:name "Type of oil"@en, "Öl"@de ;
  sh:class <http://purl.org/coscine/vocabularies/sfb985/oilType> ;
.

coscinedropShapeAnalysis:softwareVersion
  sh:path obo:IAO_0000129 ; 
  sh:order 13 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "ADVANCE software version"@en, "ADVANCE Softwareversion"@de ;
.

coscinedropShapeAnalysis:comment
  sh:path rdfs:comment ;
  sh:order 14 ;
  sh:datatype xsd:string ; 
  dash:singleLine false ;
  sh:name "Additional notes"@en, "weitere Anmerkungen"@de 
.

coscinedropShapeAnalysis:elabID
  sh:path obo:IAO_0020000 ;
  sh:order 15 ;
  sh:datatype xsd:string ; 
  sh:name "Experiment ID (eLabFTW)"@en, "Experiment-ID (eLabFTW)"@de 
.`;

export const dataCite = `@base <https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/>.

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
@prefix dcterms: <http://purl.org/dc/terms/>.
@prefix foaf: <http://xmlns.com/foaf/0.1/>.
@prefix owl: <http://www.w3.org/2002/07/owl#>.
@prefix prov: <http://www.w3.org/ns/prov#>.
@prefix schema: <http://schema.org/>.
@prefix sh: <http://www.w3.org/ns/shacl#>.
@prefix aps: <https://purl.org/coscine/ap/>.

_:autos28 rdf:first "Agricultural Sciences";
          rdf:rest _:autos29.
_:autos29 rdf:first "Arts and Humanities";
          rdf:rest _:autos30.
_:autos30 rdf:first "Astronomy and Astrophysics";
          rdf:rest _:autos31.
_:autos31 rdf:first "Business and Management";
          rdf:rest _:autos32.
_:autos32 rdf:first "Chemistry";
          rdf:rest _:autos33.
_:autos33 rdf:first "Computer and Information Science";
          rdf:rest _:autos34.
_:autos34 rdf:first "Earth and Environmental Sciences";
          rdf:rest _:autos35.
_:autos35 rdf:first "Engineering";
          rdf:rest _:autos36.
_:autos36 rdf:first "Law";
          rdf:rest _:autos37.
_:autos37 rdf:first "Mathematical Sciences";
          rdf:rest _:autos38.
_:autos38 rdf:first "Medicine, Health and Life Sciences";
          rdf:rest _:autos39.
_:autos39 rdf:first "Physics";
          rdf:rest _:autos40.
_:autos40 rdf:first "Social Sciences";
          rdf:rest _:autos41.
_:autos41 rdf:first "Other";
          rdf:rest rdf:nil.
<https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/> dcterms:created "2023-08-04"^^xsd:date;
                                                                    dcterms:creator "Sophia Leimer";
                                                                    dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html";
                                                                    dcterms:title "Minimal Dataverse Citation Metadata"@de,
                                                                                  "Minimal Dataverse Citation Metadata"@en;
                                                                    a rdfs:Class,
                                                                      sh:NodeShape;
                                                                    owl:imports <https://purl.org/coscine/ap/Dataverse_Citation_Metadata/author/>,
                                                                                <https://purl.org/coscine/ap/Dataverse_Citation_Metadata/pointOfContact/>;
                                                                    prov:wasDerivedFrom <https://purl.org/coscine/ap/0758650f-8c30-40ab-9f7d-43ab5ed73b7b/>;
                                                                    sh:closed false;
                                                                    sh:property [sh:datatype xsd:string ; 
                                                                                 sh:maxCount 1  ; 
                                                                                 sh:minCount 1  ; 
                                                                                 sh:name "Title"@en ; 
                                                                                 sh:order 1  ; 
                                                                                 sh:path dcterms:title],
                                                                                [sh:minCount 1  ; 
                                                                                 sh:name "Author"@en ; 
                                                                                 sh:order 2  ; 
                                                                                 sh:path dcterms:creator ; 
                                                                                 sh:node <https://purl.org/coscine/ap/Dataverse_Citation_Metadata/author/>],
                                                                                [sh:minCount 1  ; 
                                                                                 sh:name "Point of Contact"@en ; 
                                                                                 sh:order 3  ; 
                                                                                 sh:path schema:contactPoint ; 
                                                                                 sh:node <https://purl.org/coscine/ap/Dataverse_Citation_Metadata/pointOfContact/>],
                                                                                [sh:datatype xsd:string ; 
                                                                                 sh:minCount 1  ; 
                                                                                 sh:name "Description"@en ; 
                                                                                 sh:order 4  ; 
                                                                                 sh:path schema:description],
                                                                                [sh:maxCount 1  ; 
                                                                                 sh:minCount 1  ; 
                                                                                 sh:name "Subject"@en ; 
                                                                                 sh:order 5  ; 
                                                                                 sh:path dcterms:subject ; 
                                                                                 sh:in _:autos28].
                                                                                 
<https://purl.org/coscine/ap/Dataverse_Citation_Metadata/pointOfContact/> dcterms:created "2023-08-04"^^xsd:date;
                                                                    dcterms:creator "Sophia Leimer";
                                                                    dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html";
                                                                    dcterms:title "Dataverse Citation Metadata - Point of Contact"@de,
                                                                                  "Dataverse Citation Metadata - Point of Contact"@en;
                                                                    a rdfs:Class,
                                                                      sh:NodeShape;
                                                                    prov:wasDerivedFrom <https://purl.org/coscine/ap/Dataverse_Citation_Metadata/author/>;
                                                                    sh:closed false;
                                                                    sh:property [sh:datatype xsd:string ; 
                                                                                sh:maxCount 1  ; 
                                                                                sh:name "Point of Contact Name"@en ; 
                                                                                sh:order 8  ; 
                                                                                sh:path foaf:name],
                                                                                [sh:datatype xsd:string ; 
                                                                                sh:maxCount 1  ; 
                                                                                sh:name "Point of Contact Affiliation"@en ; 
                                                                                sh:order 10  ; 
                                                                                sh:path schema:affiliation],
                                                                                [sh:datatype xsd:string ; 
                                                                                sh:maxCount 1  ; 
                                                                                sh:name "Point of Contact E-mail"@en ; 
                                                                                sh:order 11  ; 
                                                                                sh:path schema:email ; 
                                                                                sh:minCount 1 ].
                                                                                
                                                                      _:autos14 rdf:first "ORCID";
                                                                                rdf:rest _:autos15.
                                                                      _:autos15 rdf:first "ISNI";
                                                                                rdf:rest _:autos16.
                                                                      _:autos16 rdf:first "LCNA";
                                                                                rdf:rest _:autos17.
                                                                      _:autos17 rdf:first "VIAF";
                                                                                rdf:rest _:autos18.
                                                                      _:autos18 rdf:first "GND";
                                                                                rdf:rest _:autos19.
                                                                      _:autos19 rdf:first "DAI";
                                                                                rdf:rest _:autos20.
                                                                      _:autos20 rdf:first "ResearcherID";
                                                                                rdf:rest _:autos21.
                                                                      _:autos21 rdf:first "ScopusID";
                                                                                rdf:rest rdf:nil.
                                                                      
<https://purl.org/coscine/ap/Dataverse_Citation_Metadata/author/> dcterms:created "2023-08-04"^^xsd:date;
    dcterms:creator "Sophia Leimer";
    dcterms:license "https://spdx.org/licenses/CC-BY-4.0.html";
    dcterms:title "Dataverse Citation Metadata - Author"@de,
                  "Dataverse Citation Metadata - Author"@en;
    a rdfs:Class,
      sh:NodeShape;
    prov:wasDerivedFrom <https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/>;
    sh:closed false;
    sh:property [sh:datatype xsd:string ; 
                  sh:maxCount 1  ; 
                  sh:minCount 1  ; 
                  sh:name "Author Name"@en ; 
                  sh:order 8  ; 
                  sh:path dcterms:creator],
                [sh:datatype xsd:string ; 
                  sh:maxCount 1  ; 
                  sh:name "Author Affiliation"@en ; 
                  sh:order 10  ; 
                  sh:path schema:affiliation],
                [sh:maxCount 1  ; 
                  sh:name "Author Identifier Type"@en ; 
                  sh:order 11  ; 
                  sh:path <http://purl.org/spar/datacite/AgentIdentifierScheme> ; 
                  sh:in _:autos14],
                [sh:datatype xsd:string ; 
                  sh:maxCount 1  ; 
                  sh:name "Author Identifier"@en ; 
                  sh:order 12  ; 
                  sh:path <http://purl.org/spar/datacite/AgentIdentifier>].`;

export const radarAP = `@base <https://purl.org/coscine/ap/radar/> .

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

@prefix dcterms: <http://purl.org/dc/terms/> .

@prefix coscineradar: <https://purl.org/coscine/ap/radar#> .

<https://purl.org/coscine/ap/radar/>
  dcterms:license <http://spdx.org/licenses/MIT> ;
  dcterms:publisher <https://itc.rwth-aachen.de/> ;
  dcterms:rights "Copyright © 2020 IT Center, RWTH Aachen University" ;
  dcterms:title "radar application profile"@en ;
  dcterms:description "The RADAR metadata schema v09 was created by the 2018 FIZ Karlsruhe - Leibniz-Institut fuer Informationsinfrastruktur GmbH and has been published under CC BY 4.0." ;

  a sh:NodeShape ;
  sh:targetClass <https://purl.org/coscine/ap/radar/> ;
  sh:closed true ; # no additional properties are allowed

  sh:property [
    sh:path rdf:type ;
  ] ;

  sh:property coscineradar:creator ;
  sh:property coscineradar:title ;
  sh:property coscineradar:created ;  
  sh:property coscineradar:subject ;
  sh:property coscineradar:type ;
  sh:property coscineradar:rights ;
  sh:property coscineradar:rightsHolder .

coscineradar:creator
  sh:path dcterms:creator ;
  sh:order 0 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:minLength 1 ;
  sh:datatype xsd:string ;
  sh:defaultValue "{ME}" ;
  sh:name "Creator"@en, "Ersteller"@de .

coscineradar:title
  sh:path dcterms:title ;
  sh:order 1 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:minLength 1 ;
  sh:datatype xsd:string ;
  sh:name "Title"@en, "Titel"@de .

coscineradar:created
  sh:path dcterms:created ;
  sh:order 2 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:date ;
  sh:defaultValue "{TODAY}" ;
  sh:name "Production Date"@en, "Erstelldatum"@de .

coscineradar:subject
  sh:path dcterms:subject ;
  sh:order 3 ;
  sh:maxCount 1 ;
  sh:name "Subject Area"@en, "Fachrichtung"@de ;
  sh:class <http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/> .

coscineradar:type
  sh:path dcterms:type ;
  sh:order 4 ;
  sh:maxCount 1 ;
  sh:name "Resource"@en, "Ressource"@de ;
  sh:class <http://purl.org/dc/dcmitype/> .

coscineradar:rights
  sh:path dcterms:rights ;
  sh:order 5 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Rights"@en, "Berechtigung"@de .

coscineradar:rightsHolder
  sh:path dcterms:rightsHolder ;
  sh:order 6 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Rightsholder"@en, "Rechteinhaber"@de .`;
